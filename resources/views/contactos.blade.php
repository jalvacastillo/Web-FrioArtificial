<div class="parallax-container valign-wrapper section scrollspy" id="contactos">
  <div class="section no-pad-bot">
    <div class="container">
      <div class="row center">
          <h5 class="header col s12 light">
            Contamos con una nueva linea ecológica y variedad de productos y servicios
            <br><br>
          </h5>
            <!-- Modal Trigger -->
              <a class="waves-effect waves-light btn modal-trigger" href="#form">Escribenos</a>
            
      </div>
    </div>
  </div>
  <div class="parallax"><img src="/img/background2.jpg" alt="Unsplashed background img 2"></div>
</div>


        <!-- Modal Structure -->
        <div id="form" class="modal modal-fixed-footer row" ng-controller="FormCtrl">
          <form role="form" method="POST" ng-submit="email(correo);">
                {{-- {!! csrf_field() !!} --}}
              <div class="modal-content">
                <h4 class="section-heading center">Gracias por escribirnos</h4>
                    <p>
                      @{{emailErrores}}
                    </p>
                    <div class="input-field col s12">
                        <input id="nombre" type="text" class="validate" required ng-model="correo.nombre">
                        <label for="nombre">Soy</label>
                    </div>
                    
                    <div class="input-field col s12">
                      <input id="email" name="email" type="email" class="validate" required ng-model="correo.correo">
                      <label for="email" name="email">Mi correo es</label>
                    </div>
                    
                    <div class="input-field col s12">
                        <textarea id="mensaje" class="materialize-textarea" required ng-model="correo.mensaje"></textarea>
                        <label for="mensaje">Quiero:</label>
                    </div>

              </div>
              <div class="modal-footer">
                    <button class="right modal-action waves-effect waves-light btn" type="submit" name="action">
                      <span ng-show="!loader">Enviar</span>
                      <span ng-show="loader">Enviando...</span>
                    </button>
                    <a href="#!" class="left modal-close waves-effect waves-green btn-flat ">Cancelar</a>
              </div>
            </form>
        </div>

