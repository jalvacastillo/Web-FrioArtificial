<nav class="white" role="navigation">
  <div class="nav-wrapper container">
    <a id="logo-container" href="#index-banner" class="brand-logo">
      <img src="/img/logo.png" alt="Logotipo" width="200px" style="margin-top: 10px;">
    </a>
    <ul class="right hide-on-med-and-down">
      <li><a href="#servicios">Servicios</a></li>
      <li><a href="#contactos">Contactos</a></li>
    </ul>

    <ul id="nav-mobile" class="side-nav">
      <li><a href="#servicios">Servicios</a></li>
      <li><a href="#contactos">Contactos</a></li>
    </ul>
    <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
  </div>
</nav>