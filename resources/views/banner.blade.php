<div id="index-banner" class="parallax-container section scrollspy">
  <div class="section no-pad-bot">
    <div class="container">
      <br><br>
      <h1 class="header center teal-text text-darken-4">Frio Artificial</h1>
      <div class="row center">
        <h5 class="header col s12 light">
            Instalación - Mantenimiento - Reparación
        </h5>
        <p class="center teal-text text-darken-1">de Aires Acondicionados</p>
      </div>
      <div class="row center">
        <a href="#servicios" id="download-button" class="btn-large waves-effect waves-light teal lighten-1">Servicios</a>
      </div>
      <br><br>

    </div>
  </div>
  <div class="parallax"><img src="/img/background1.jpg" alt="Unsplashed background img 1"></div>
</div>