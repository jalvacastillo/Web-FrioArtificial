<!DOCTYPE html>
<html lang="en" ng-app="app">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <title>Frio Artificial</title>
    <link type="image/x-icon" href="/favicon.ico" rel="shortcut icon">

    {{-- Seo --}}
    <meta name="description" content="Somos una Empresa dedicada a la venta e instalación de equipos de aire acondicionados y al servicio de mantenimiento preventivo y correctivo.">
    <meta name="keywords" content="aire,acondicionado,aire acondicionado, mantenimiento, mantenimiento aires, reparacion aires, venta aires acondicionados">
    <meta name="author" content="CDMYPE Ilobasco - Asesor TIC (Jesus Alvarado)">
    <meta name="HandheldFriendly" content="True">

    <!-- CSS  -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

</head>
<body>

    @include('header')  
    @include('banner')  
    @include('servicios')
    @include('contactos')
    @include('footer')


    <!--  Scripts-->
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/controller.js"></script>
    <script src="js/materialize.min.js"></script>
    <script src="js/init.js"></script>

</body>
</html>
