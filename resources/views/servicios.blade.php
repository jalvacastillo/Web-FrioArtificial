<div class="container">
  <div class="section scrollspy" id="servicios">

    <!--   Icon Section   -->
    <div class="row">
      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center brown-text"><i class="material-icons">card_membership</i></h2>
          <h5 class="center">Instalación</h5>

          <p class="light">
            Ofrecemos la venta e instalación de equipos residenciales y comerciales.
          </p>
          <ul class="collection">
                <li class="collection-item">Presupuesto</li>
                <li class="collection-item">Cálculo y diseño</li>
                <li class="collection-item">Venta e instalación</li>
          </ul>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center brown-text"><i class="material-icons">group</i></h2>
          <h5 class="center">Mantenimiento</h5>

          <p class="light">
            Para que su equipo rinda y dure más, ofrecemos:
          </p>
          <ul class="collection">
                <li class="collection-item">Limpieza general</li>
                <li class="collection-item">Verificación del sistema</li>
                <li class="collection-item">Planes de mantenimiento</li>
          </ul>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h2 class="center brown-text"><i class="material-icons">build</i></h2>
          <h5 class="center">Reparación</h5>

           <p class="light">
            Tenenos personal calificado y con experiencia en:
          </p>
          <ul class="collection">
                <li class="collection-item">Reparación Eléctrica</li>
                <li class="collection-item">Reparación Mecánica</li>
                <li class="collection-item">Traslados</li>
          </ul>
        </div>
      </div>
    </div>

  </div>
</div>