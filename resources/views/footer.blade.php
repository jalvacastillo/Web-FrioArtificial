<footer class="page-footer teal">
  <div class="container">
    <div class="row">
      <div class="col l6 s12">
        <h5 class="white-text">Sobre nosotros</h5>
        <p class="grey-text text-lighten-4">Somos una Empresa dedicada a la venta e instalación de equipos de aire acondicionados y al servicio de mantenimiento preventivo y correctivo.</p>


      </div>
      <div class="col l3 s12">
        <h5 class="white-text">Contactos</h5>
        <ul>
          <li><a class="white-text" href="tel:7094-6766">(503) 7094-6766</a></li>
          <li><a class="white-text" href="#!">3° Calle Ote. No 10,</a></li>
          <li><a class="white-text" href="#!">Bo. Santa Lucia,  </a></li>
          <li><a class="white-text" href="#!">Cojutepeque, Cuscatlan</a></li>
        </ul>
      </div>
      <div class="col l3 s12">
        <h5 class="white-text">Siguenos</h5>
        <ul>
          <li><a class="white-text" href="#!">Facebook</a></li>
          <li><a class="white-text" href="#!">Instagram</a></li>
          <li><a class="white-text" href="mailto:ventas@frioartificial.com">ventas@frioartificial.com</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright">
    <div class="container">
      By <a class="brown-text text-lighten-3" href="http://cri.catolica.edu.sv/cdmype">CDMYPE</a>
    </div>
  </div>
</footer>