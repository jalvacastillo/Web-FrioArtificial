(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.parallax').parallax();
    $('.scrollspy').scrollSpy({
        scrollOffset: 50
    });
    $('.modal-trigger').leanModal();

  }); // end of document ready
})(jQuery); // end of jQuery name space