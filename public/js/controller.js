"use strict";

angular.module('app', [])

.constant("config", {
    "url": "http://localhost:8000/"
})

.controller('FormCtrl', ['$scope', '$http', 'config', function ($scope, $http, config) {

    $scope.loader = false;
    $scope.correo = "";

    $scope.email = function(correo){
        console.log(correo);
        if($scope.correo){
            $scope.loader = true;
            $http.post(config.url + 'correo', $scope.correo).
              success(function(data, status) {
                if (status == 200) {
                    $scope.correo = {};
                    $scope.loader = false;
                    $('#form').closeModal();
                }else{
                    $scope.loader = false;
                    $scope.emailErrores = data;
                }
              }).
              error(function(data, status) {
                $scope.loader = false;
                $scope.emailErrores = ["El mensaje no pudo ser enviado, revice su conexión a internet."];
              });
        }
    }
    
}])

;