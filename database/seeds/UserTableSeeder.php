<?php

use Illuminate\Database\Seeder;
use app\User;

class UserTableSeeder extends Seeder {
    public function run(){
        User::create(array(
            'email'     => 'admin@admin.com',
            'name'  => 'Jesus Alfonso Alvarado Castillo',
            'password' => Hash::make('admin'),
            'tipo'  => '1'
        ));
    }
}