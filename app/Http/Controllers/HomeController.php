<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Mail;

class HomeController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function index()
    {
        return view('wish.index');
    }

    public function correo(Request $Request)
    {   

        try {
            Mail::send('emails.contacto', ['cliente' => $Request], function ($m) use ($Request) {
                $m->to('alvarado.websis@gmail.com', 'Jesús Alvarado')
                  ->subject('Frio Artificial');
            });
            return response()->json(['msj' => $Request]);
        } catch (Exception $e) {
            return response()->json(['msj' => $e]);
        }

    }
    
}
